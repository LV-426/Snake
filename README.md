# Snake
Simple game of snake, written using PyGame

# Install 

```normal
virtualenv Pygame-env && cd Pygame-env
source bin/activate
git clone https://codeberg.org/LV-426/Snake.git
cd Snake
pip3 install -r requirements.txt
python3 snake.py
```

# TODO

- [x] Add scoring system
- [x] Save scores to a file.
- [x] UI
