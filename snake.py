import pygame, time, random, os
from pygame import mixer

pygame.init()

# Set path
dirPath = os.path.dirname(os.path.realpath(__file__))

pygame.display.set_caption('Snake!')
mainClock = pygame.time.Clock()

green = (50,205,50)
yellow = (255, 255, 0)
red = (255,0,0)
black = (0,0,0)

frameRate = 10

surfaceSize = 600, 600

if pygame.get_init():
    print("Success")
else:
    print("Critical, exiting")
    exit()

def snakeDrawing(snakeBlock, snakeList, foodx, foody, surface):
    theCount = 1
    for x in snakeList:
        if theCount >= len(snakeList):
            snake = pygame.draw.rect(surface, green, [x[0], x[1], snakeBlock, snakeBlock])
        else:
            pygame.draw.rect(surface, red, [x[0], x[1], snakeBlock, snakeBlock])
        theCount += 1

def spawnFood(foodx, foody, snakeList, surfaceSize):
    bGood = True
    while bGood:
        foodx = random.randrange(0,surfaceSize[0]-25+1,25)
        foody = random.randrange(0,surfaceSize[1]-25+1,25)
        newlist = []
        newlist.append(foodx)
        newlist.append(foody)
        for x in snakeList:
            if x == newlist:
                bGood = True
                break
            else:
                bGood = False
    return foodx, foody

def startGame():
    surface = pygame.display.set_mode(surfaceSize)
    surface.fill(black)
    button = pygame.Rect(surfaceSize[0] // 2-75, surfaceSize[1] // 2-100, 150, 50)  # creates a rect object
    button2 = pygame.Rect(surfaceSize[0] // 2-75, surfaceSize[1] // 2-25, 150, 50)  # creates a rect object

    font = pygame.font.Font('freesansbold.ttf', 50)

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return False
            if event.type == pygame.MOUSEBUTTONDOWN:
                mouse_pos = event.pos  # gets mouse position
                if button.collidepoint(mouse_pos):
                    gameLoop(surface)
                if button2.collidepoint(mouse_pos):
                    exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    pygame.event.post(pygame.event.Event(pygame.QUIT))
                if event.key == pygame.K_SPACE:
                    gameLoop(surface)
            pygame.draw.rect(surface, red, button)
            pygame.draw.rect(surface, red, button2)
            text = font.render("Play", True, black)
            text2 = font.render("Quit", True, black)
            surface.blit(text, button)
            surface.blit(text2, button2)
            pygame.display.update()

def endGame(surface, score):
    surface.fill(black)
    button = pygame.Rect(surfaceSize[0] // 2-75, surfaceSize[1] // 2-100, 150, 50)  # creates a rect object
    button2 = pygame.Rect(surfaceSize[0] // 2-75, surfaceSize[1] // 2-25, 150, 50)  # creates a rect object

    font = pygame.font.Font('freesansbold.ttf', 50)
    font2 = pygame.font.Font('freesansbold.ttf', 25)

    textScore = font2.render("Score: "+str(score), True, red)
    textRect = textScore.get_rect()
    textRect.center = (surfaceSize[0] // 2, surfaceSize[1] // 2-175)

    if score > 0:
        with open(dirPath+'/scores.txt', 'a+') as file:
            file.write(str(score)+"\n")
            file.close()

    while True:
        pygame.draw.rect(surface, red, button)
        pygame.draw.rect(surface, red, button2)
        text = font.render("Play", True, black)
        text2 = font.render("Quit", True, black)
        surface.blit(text, button)
        surface.blit(text2, button2)
        surface.blit(textScore, textRect)
        pygame.display.update()

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return False
            if event.type == pygame.MOUSEBUTTONDOWN:
                mouse_pos = event.pos  # gets mouse position
                if button.collidepoint(mouse_pos):
                    gameLoop(surface)
                if button2.collidepoint(mouse_pos):
                    exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    pygame.event.post(pygame.event.Event(pygame.QUIT))
                    exit()
                if event.key == pygame.K_SPACE:
                    gameLoop(surface)

def gameLoop(surface):

    score = 0

    foodx = 0
    foody = 0

    font = pygame.font.Font('freesansbold.ttf', 32)
    text = font.render("Score: "+str(score), True, green)
    textRect = text.get_rect()
    textRect.center = (surfaceSize[0]-535, surfaceSize[1]-580)

    snakeBlock = 25

    surface.fill(black)

    snake_x_d = 0
    snake_y_d = 0

    lengthOfSnake = 3
    snake = pygame.draw.rect(surface, green, pygame.Rect(snake_x_d, snake_y_d, snakeBlock, snakeBlock))
    snakeList = []
    snakeHead = []
    snakeHead.append(snake.x)
    snakeHead.append(snake.y)
    snakeList.append(snakeHead)

    snake_x = 0
    snake_y = 25

    bCanPressKey = True

    foodx, foody = spawnFood(foodx, foody, snakeList, surfaceSize)

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                exit()

            if bCanPressKey:
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        pygame.event.post(pygame.event.Event(pygame.QUIT))
                    if event.key == pygame.K_UP:
                        if snake_y != 25:
                            snake_x = 0
                            snake_y = -25
                    if event.key == pygame.K_DOWN:
                        if snake_y != -25:
                            snake_x = 0
                            snake_y = 25
                    if event.key == pygame.K_LEFT:
                        if snake_x != 25:
                            snake_x = -25
                            snake_y = 0
                    if event.key == pygame.K_RIGHT:
                        if snake_x != -25:
                            snake_x = 25
                            snake_y = 0
                    bCanPressKey = False

        if len(snakeList) > lengthOfSnake:
            del snakeList[0]

        snake.move_ip(snake_x, snake_y)
        surface.fill(black)

        snakeHead = []
        snakeHead.append(snake.x)
        snakeHead.append(snake.y)
        snakeList.append(snakeHead)

        snakeDrawing(snakeBlock, snakeList, foodx, foody, surface)
        bCanPressKey = True

        for x in snakeList[:-1]:
            if x == snakeHead:
                endGame(surface, score)

        if snake.x >= surfaceSize[0] or snake.x < 0 or snake.y >= surfaceSize[1] or snake.y < 0:
            endGame(surface, score)

        if snake.y == foody and snake.x == foodx:
            foodx, foody = spawnFood(foodx, foody, snakeList, surfaceSize)
            lengthOfSnake += 1
            score += 1
            text = font.render("Score: "+str(score), True, green)

        surface.blit(text, textRect)
        pygame.draw.rect(surface, yellow, [foodx, foody, snakeBlock, snakeBlock])

        pygame.display.update()
        mainClock.tick(frameRate)

startGame()
